# BlocklyOrangePi

#### 项目介绍
本项目基于Blockly对香橙派开发板进行了改进和适配，毕竟近几年树莓派的价格实在是太贵了

#### 软件架构
基于HTML，Javascript的在线编辑器，模块调用部分采用了C语言调用wiringop库导出python接口，相比读写gpio设备文件性能更强

#### 使用说明
见项目 `apps/orangepiblockly/index.html` 地址

默认支持OrangePiZero2，需要其他型号支持修改pi/define_pins.js的请自行添加pin映射表

使用前先`cd wiringop_libraries`然后执行`make && make install`安装wiringOP函数库

进行串口通信时需要先执行sudo apt-get install python-serial安装相应的扩展包

使用iic模块时需要开启iic功能并执行sudo apt-get install python-smbus安装相应的扩展包

然后在`python_libraries`的子目录里执行`python setup.py install`安装对应的扩展库

#### 使用预览
见链接：[https://hvwyl.gitee.io/BlocklyOrangePi](https://hvwyl.gitee.io/BlocklyOrangePi)
![使用预览](https://gitee.com/hvhgc/BlocklyOrangePi/raw/master/images/pic1.png "pic1.png")

#### 模块调用函数库说明

GPIO的驱动库python_libraries/GPIO目录下

ADS1115电压传感器的驱动库在python_libraries/ads1115目录下

BMP180气压/温度传感器的驱动库在python_libraries/bmp180目录下

DS18B20温度传感器的驱动库在python_libraries/ds18b20目录下

LCD1602的驱动库在python_libraries/lcd1602目录下

OLED的驱动库在python_libraries/ssd1306目录下

TFT彩屏的驱动库在python_libraries/st7735s目录下

步进电机的驱动库在python_libraries/stepper目录下

TM1637数码管的驱动库在python_libraries/tm1637目录下

另外，在对GPIO实时性要求不高的场景下可以使用经典的89c52通过串口连接香橙派作为下位机来扩展出30个数字io，驱动库和相关资料在python_libraries/pyc51目录下

其中，GPIO、DS18B20、ST7735S的驱动由C语言完成，ADS1115、BMP180、LCD1602、SSD1306、Stepper、TM1637由python语言完成，pyc51由python和8051c语言完成

#### 软件完善计划

- [x] 用C语言重写GPIO接口并与旧的代码形式上兼容
- [x] 支持更多的硬件模块
- [x] 文件系统模块、基础网络模块
- [ ] 基于在线编程运行和webshell管理平台
- [ ] 系统初始化和配网系统
- [ ] <s>多机器集群部署和完善的通信方案</s>
- [ ] 待补充。。。

#### 硬件完善计划

默认基于h616的OrangePiZero2完成，因为手头只有这块板子，RPi3b已经在疫情期间卖了\[手动笑哭\]
- [ ] 自定义硬件连接跳线和扩展版
- [ ] 待补充。。。