Blockly.Python.slave_pins=function(){
	Blockly.Python.definitions_['import pyc51'] = "import pyc51";
	return [this.getFieldValue("PIN"),Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.slave_init=function(){
    Blockly.Python.definitions_['import pyc51'] = "import pyc51";
    var DEV_FILE=Blockly.Python.valueToCode(this,'DEV_FILE',Blockly.Python.ORDER_ATOMIC);
	return ["pyc51.C51("+DEV_FILE+")",Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.slave_output=function(){
    Blockly.Python.definitions_['import pyc51'] = "import pyc51";
    var SLAVE_OBJ=Blockly.Python.valueToCode(this,'SLAVE_OBJ',Blockly.Python.ORDER_ATOMIC);
    var PIN=Blockly.Python.valueToCode(this,'PIN',Blockly.Python.ORDER_ATOMIC);
    var STAT=Blockly.Python.valueToCode(this,'STAT',Blockly.Python.ORDER_ATOMIC);
	return SLAVE_OBJ+".output("+PIN+","+STAT+")\n";
};

Blockly.Python.slave_input=function(){
    Blockly.Python.definitions_['import pyc51'] = "import pyc51";
    var SLAVE_OBJ=Blockly.Python.valueToCode(this,'SLAVE_OBJ',Blockly.Python.ORDER_ATOMIC);
    var PIN=Blockly.Python.valueToCode(this,'PIN',Blockly.Python.ORDER_ATOMIC);
	return [SLAVE_OBJ+".input("+PIN+")",Blockly.Python.ORDER_ATOMIC];
};