Blockly.Python.display_lcd1602_init=function(){
    Blockly.Python.definitions_['from lcd1602 import LCD1602'] = "from lcd1602 import LCD1602";
    var bus=Blockly.Python.valueToCode(this,'BUS',Blockly.Python.ORDER_ATOMIC);
    var lcd_addr=Blockly.Python.valueToCode(this,'LCD_ADDR',Blockly.Python.ORDER_ATOMIC);
	return ["LCD1602("+bus+","+lcd_addr+")",Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.display_lcd1602_print=function(){
    Blockly.Python.definitions_['from lcd1602 import LCD1602'] = "from lcd1602 import LCD1602";
    var lcd_obj=Blockly.Python.valueToCode(this,'LCD_OBJ',Blockly.Python.ORDER_ATOMIC);
    var x=Blockly.Python.valueToCode(this,'x',Blockly.Python.ORDER_ATOMIC);
    var y=Blockly.Python.valueToCode(this,'y',Blockly.Python.ORDER_ATOMIC);
    var str=Blockly.Python.valueToCode(this,'STR',Blockly.Python.ORDER_ATOMIC);
	return lcd_obj+".print_lcd("+y+"-1,"+x+"-1,"+str+")\n";
};

Blockly.Python.display_lcd1602_printline=function(){
    Blockly.Python.definitions_['from lcd1602 import LCD1602'] = "from lcd1602 import LCD1602";
    var lcd_obj=Blockly.Python.valueToCode(this,'LCD_OBJ',Blockly.Python.ORDER_ATOMIC);
    var str1=Blockly.Python.valueToCode(this,'STR1',Blockly.Python.ORDER_ATOMIC);
    var str2=Blockly.Python.valueToCode(this,'STR2',Blockly.Python.ORDER_ATOMIC);
	return lcd_obj+".print_lcd(0,0,"+str1+")\n"+lcd_obj+".print_lcd(0,1,"+str2+")\n";
};

Blockly.Python.display_lcd1602_clear=function(){
    Blockly.Python.definitions_['from lcd1602 import LCD1602'] = "from lcd1602 import LCD1602";
    var lcd_obj=Blockly.Python.valueToCode(this,'LCD_OBJ',Blockly.Python.ORDER_ATOMIC);
	return lcd_obj+".clear_lcd()\n";
};

Blockly.Python.display_oled_init=function(){
    Blockly.Python.definitions_['from ssd1306 import SSD1306'] = "from ssd1306 import SSD1306";
    var bus=Blockly.Python.valueToCode(this,'BUS',Blockly.Python.ORDER_ATOMIC);
    var oled_addr=Blockly.Python.valueToCode(this,'OLED_ADDR',Blockly.Python.ORDER_ATOMIC);
	return ["SSD1306("+bus+","+oled_addr+")",Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.display_oled_print=function(){
    Blockly.Python.definitions_['from ssd1306 import SSD1306'] = "from ssd1306 import SSD1306";
    var oled_obj=Blockly.Python.valueToCode(this,'OLED_OBJ',Blockly.Python.ORDER_ATOMIC);
    var x=Blockly.Python.valueToCode(this,'x',Blockly.Python.ORDER_ATOMIC);
    var y=Blockly.Python.valueToCode(this,'y',Blockly.Python.ORDER_ATOMIC);
    var str=Blockly.Python.valueToCode(this,'STR',Blockly.Python.ORDER_ATOMIC);
	return oled_obj+".ShowString("+x+","+y+","+str+")\n";
};

Blockly.Python.display_oled_printline=function(){
    Blockly.Python.definitions_['from ssd1306 import SSD1306'] = "from ssd1306 import SSD1306";
    var oled_obj=Blockly.Python.valueToCode(this,'OLED_OBJ',Blockly.Python.ORDER_ATOMIC);
    var str1=Blockly.Python.valueToCode(this,'STR1',Blockly.Python.ORDER_ATOMIC);
    var str2=Blockly.Python.valueToCode(this,'STR2',Blockly.Python.ORDER_ATOMIC);
    var str3=Blockly.Python.valueToCode(this,'STR3',Blockly.Python.ORDER_ATOMIC);
    var str4=Blockly.Python.valueToCode(this,'STR4',Blockly.Python.ORDER_ATOMIC);
	return oled_obj+".ShowString(0,0,"+str1+")\n"+oled_obj+".ShowString(0,2,"+str2+")\n"+oled_obj+".ShowString(0,4,"+str3+")\n"+oled_obj+".ShowString(0,6,"+str4+")\n";
};

Blockly.Python.display_oled_clear=function(){
    Blockly.Python.definitions_['from ssd1306 import SSD1306'] = "from ssd1306 import SSD1306";
    var oled_obj=Blockly.Python.valueToCode(this,'OLED_OBJ',Blockly.Python.ORDER_ATOMIC);
	return oled_obj+".Clear()\n";
};

Blockly.Python.display_tft_init=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['import st7735s'] = "import st7735s";
    var spidev=Blockly.Python.valueToCode(this,'SPIDEV',Blockly.Python.ORDER_ATOMIC);
    var res=Blockly.Python.valueToCode(this,'RES',Blockly.Python.ORDER_ATOMIC);
    var dc=Blockly.Python.valueToCode(this,'DC',Blockly.Python.ORDER_ATOMIC);
    var speed=Blockly.Python.valueToCode(this,'SPEED',Blockly.Python.ORDER_ATOMIC);
	return ["st7735s.ST7735S(spidev="+spidev+",RES="+res+",DC="+dc+",SPEED="+speed+")",Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.display_tft_clear=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['import st7735s'] = "import st7735s";
    var tft_obj=Blockly.Python.valueToCode(this,'TFT_OBJ',Blockly.Python.ORDER_ATOMIC);
    var colour=Blockly.Python.valueToCode(this,'COLOUR',Blockly.Python.ORDER_ATOMIC);
	return tft_obj+".Clear(st7735s.c888to565("+colour+"))\n";
};

Blockly.Python.display_tft_print=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['import st7735s'] = "import st7735s";
    var tft_obj=Blockly.Python.valueToCode(this,'TFT_OBJ',Blockly.Python.ORDER_ATOMIC);
    var x=Blockly.Python.valueToCode(this,'x',Blockly.Python.ORDER_ATOMIC);
    var y=Blockly.Python.valueToCode(this,'y',Blockly.Python.ORDER_ATOMIC);
    var str=Blockly.Python.valueToCode(this,'STR',Blockly.Python.ORDER_ATOMIC);
    var front_colour=Blockly.Python.valueToCode(this,'FRONT_COLOUR',Blockly.Python.ORDER_ATOMIC);
    var back_colour=Blockly.Python.valueToCode(this,'BACK_COLOUR',Blockly.Python.ORDER_ATOMIC);
	return tft_obj+".DrawString("+x+","+y+","+str+",st7735s.c888to565("+front_colour+"),st7735s.c888to565("+back_colour+"))\n";
};

Blockly.Python.display_tft_drawpoint=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['import st7735s'] = "import st7735s";
    var tft_obj=Blockly.Python.valueToCode(this,'TFT_OBJ',Blockly.Python.ORDER_ATOMIC);
    var x=Blockly.Python.valueToCode(this,'x',Blockly.Python.ORDER_ATOMIC);
    var y=Blockly.Python.valueToCode(this,'y',Blockly.Python.ORDER_ATOMIC);
    var colour=Blockly.Python.valueToCode(this,'COLOUR',Blockly.Python.ORDER_ATOMIC);
	return tft_obj+".DrawPoint("+x+","+y+",st7735s.c888to565("+colour+"))\n";
};

Blockly.Python.display_tft_drawrect=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['import st7735s'] = "import st7735s";
    var tft_obj=Blockly.Python.valueToCode(this,'TFT_OBJ',Blockly.Python.ORDER_ATOMIC);
    var x1=Blockly.Python.valueToCode(this,'x1',Blockly.Python.ORDER_ATOMIC);
    var y1=Blockly.Python.valueToCode(this,'y1',Blockly.Python.ORDER_ATOMIC);
    var x2=Blockly.Python.valueToCode(this,'x2',Blockly.Python.ORDER_ATOMIC);
    var y2=Blockly.Python.valueToCode(this,'y2',Blockly.Python.ORDER_ATOMIC);
    var colour=Blockly.Python.valueToCode(this,'COLOUR',Blockly.Python.ORDER_ATOMIC);
	return tft_obj+".DrawRect("+x1+","+y1+","+x2+","+y2+",st7735s.c888to565("+colour+"))\n";
};

Blockly.Python.display_tft_drawline=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['import st7735s'] = "import st7735s";
    var tft_obj=Blockly.Python.valueToCode(this,'TFT_OBJ',Blockly.Python.ORDER_ATOMIC);
    var x1=Blockly.Python.valueToCode(this,'x1',Blockly.Python.ORDER_ATOMIC);
    var y1=Blockly.Python.valueToCode(this,'y1',Blockly.Python.ORDER_ATOMIC);
    var x2=Blockly.Python.valueToCode(this,'x2',Blockly.Python.ORDER_ATOMIC);
    var y2=Blockly.Python.valueToCode(this,'y2',Blockly.Python.ORDER_ATOMIC);
    var colour=Blockly.Python.valueToCode(this,'COLOUR',Blockly.Python.ORDER_ATOMIC);
	return tft_obj+".DrawLine("+x1+","+y1+","+x2+","+y2+",st7735s.c888to565("+colour+"))\n";
};

Blockly.Python.display_tm1637_init=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['from tm1637 import TM1637'] = "from tm1637 import TM1637";
    var clk=Blockly.Python.valueToCode(this,'CLK',Blockly.Python.ORDER_ATOMIC);
    var dio=Blockly.Python.valueToCode(this,'DIO',Blockly.Python.ORDER_ATOMIC);
	return ["TM1637("+clk+","+dio+")",Blockly.Python.ORDER_ATOMIC];
};

Blockly.Python.display_tm1637_number=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['from tm1637 import TM1637'] = "from tm1637 import TM1637";
    var led_obj=Blockly.Python.valueToCode(this,'LED_OBJ',Blockly.Python.ORDER_ATOMIC);
    var num=Blockly.Python.valueToCode(this,'NUM',Blockly.Python.ORDER_ATOMIC);
	return led_obj+".number("+num+")\n";
};

Blockly.Python.display_tm1637_print=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['from tm1637 import TM1637'] = "from tm1637 import TM1637";
    var led_obj=Blockly.Python.valueToCode(this,'LED_OBJ',Blockly.Python.ORDER_ATOMIC);
    var mode=this.getFieldValue('MODE')=="TRUE"?"True":"False";
    var str=Blockly.Python.valueToCode(this,'STR',Blockly.Python.ORDER_ATOMIC);
	return led_obj+".show("+str+","+mode+")\n";
};

Blockly.Python.display_tm1637_ratio=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['from tm1637 import TM1637'] = "from tm1637 import TM1637";
    var led_obj=Blockly.Python.valueToCode(this,'LED_OBJ',Blockly.Python.ORDER_ATOMIC);
    var num1=Blockly.Python.valueToCode(this,'NUM1',Blockly.Python.ORDER_ATOMIC);
    var num2=Blockly.Python.valueToCode(this,'NUM2',Blockly.Python.ORDER_ATOMIC);
	return led_obj+".numbers("+num1+","+num2+")\n";
};

Blockly.Python.display_tm1637_temperature=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['from tm1637 import TM1637'] = "from tm1637 import TM1637";
    var led_obj=Blockly.Python.valueToCode(this,'LED_OBJ',Blockly.Python.ORDER_ATOMIC);
    var temp=Blockly.Python.valueToCode(this,'TEMP',Blockly.Python.ORDER_ATOMIC);
	return led_obj+".temperature("+temp+")\n";
};

Blockly.Python.display_tm1637_clear=function(){
    Blockly.Python.definitions_['import GPIO'] = "import GPIO\nGPIO.init()";
    Blockly.Python.definitions_['from tm1637 import TM1637'] = "from tm1637 import TM1637";
    var led_obj=Blockly.Python.valueToCode(this,'LED_OBJ',Blockly.Python.ORDER_ATOMIC);
	return led_obj+".show('    ',False)\n";
};