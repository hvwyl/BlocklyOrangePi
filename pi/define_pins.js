OBJ_FLAG="_";

var profile = {
    Zero2: {
        description: "Physics Pins",
        digital: [["7", "7"],["11", "11"],["12", "12"],["13", "13"],["15", "15"],["16", "16"],["22", "22"],["26", "26"]],
        i2cbus: [["3","3"]],
        spidev: [["/dev/spidev1.1","/dev/spidev1.1"]]
    },
};

profile["default"] = profile.Zero2;