//--hidden module--
Blockly.Blocks.factory_i2cbus={
    init:function(){
        this.setColour(75);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldDropdown(profile.default.i2cbus),"VALUE");
        this.setOutput(true,"Number");
    }
};
Blockly.Blocks.factory_spidev={
    init:function(){
        this.setColour(75);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldDropdown(profile.default.spidev),"VALUE");
        this.setOutput(true,"String");
    }
};
//--obvious module--
Blockly.Blocks.factory_import={
    init:function(){
        this.setColour(75);
        this.appendDummyInput("")
            .appendField("import ")
            .appendField(new Blockly.FieldTextInput('time'), 'IMPORT');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

Blockly.Blocks.factory_do={
    init:function(){
        this.setColour(75);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('print(\"Hello world!\")'),'VALUE');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

Blockly.Blocks.factory_block={
    init:function(){
        this.setColour(75);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('def func()'),'VALUE')
            .appendField(":");
        this.appendStatementInput('DO');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

Blockly.Blocks.factory_var={
    init:function(){
        this.setColour(75);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldTextInput('test'), 'VALUE');
        this.setOutput(true);
    }
};