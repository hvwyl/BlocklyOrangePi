Blockly.Blocks.slave_pins={
	init:function(){
        this.setColour(20);
        var pins=[];
        for(let i=0;i<=3;i++){
            for(let j=0;j<=7;j++){
                if(i==3&j==0 | i==3&j==1){}else{
                    pins.push(["P"+i+"."+j,"0x"+i+j]);
                };
            };
        };
		this.appendDummyInput()
			.appendField(new Blockly.FieldDropdown(pins),"PIN");
		this.setOutput(true,"Number");
	}
};

Blockly.Blocks.slave_init={
    init:function(){
	    this.setColour(20);
	    this.appendDummyInput()
            .appendField("51单片机 初始化")
        this.appendValueInput("DEV_FILE")
            .appendField("设备")
            .setCheck("String");
        this.setOutput(true);
    }
};

Blockly.Blocks.slave_output={
    init:function(){
	    this.setColour(20);
	    this.appendValueInput("SLAVE_OBJ")
            .appendField("数字输出 51单片机")
        this.appendValueInput("PIN")
            .appendField("管脚#");
        this.appendValueInput("STAT")
            .appendField("设为");
        this.setPreviousStatement(true);
	    this.setNextStatement(true);
        this.setInputsInline(true);
    }
};

Blockly.Blocks.slave_input={
    init:function(){
	    this.setColour(20);
	    this.appendValueInput("SLAVE_OBJ")
            .appendField("数字输入 51单片机")
        this.appendValueInput("PIN")
            .appendField("管脚#");
        this.setOutput(true);
        this.setInputsInline(true);
    }
};