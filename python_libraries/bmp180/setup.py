from distutils.core import setup
setup(
    name='bmp180',
    version='1.0',
    description='bmp180 library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    py_modules=['bmp180']
)