import serial

class C51:
    def __init__(self,dev):
        self.ser=serial.Serial(dev,9600,timeout=0.5)

    def output(self,pin,value):
        if value==1 or value==0:
            if pin<=0x37 and pin>=0x00 and pin!=0x30 and pin!=0x31:
                self.ser.write(chr(value)+chr(pin))
                return True

    def input(self,pin):
        self.ser.flushInput()
        if pin<=0x37 and pin>=0x00 and pin!=0x30 and pin!=0x31:
            self.ser.write(chr(0x02)+chr(pin))
            result=self.ser.read(1)
            if result=="\x00":
                return 0
            elif result=="\x01":
                return 1