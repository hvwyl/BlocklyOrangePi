#include<reg52.h>

sbit P00=P0^0;
sbit P01=P0^1;
sbit P02=P0^2;
sbit P03=P0^3;
sbit P04=P0^4;
sbit P05=P0^5;
sbit P06=P0^6;
sbit P07=P0^7;

sbit P10=P1^0;
sbit P11=P1^1;
sbit P12=P1^2;
sbit P13=P1^3;
sbit P14=P1^4;
sbit P15=P1^5;
sbit P16=P1^6;
sbit P17=P1^7;

sbit P20=P2^0;
sbit P21=P2^1;
sbit P22=P2^2;
sbit P23=P2^3;
sbit P24=P2^4;
sbit P25=P2^5;
sbit P26=P2^6;
sbit P27=P2^7;

sbit P32=P3^2;
sbit P33=P3^3;
sbit P34=P3^4;
sbit P35=P3^5;
sbit P36=P3^6;
sbit P37=P3^7;

int status=0;

int command=0xFF;
int param=0xFF;

void uart_init()
{
    SCON=0X50;
    TMOD=0x20;
	TH1=0xFD;//Baud 9600
	TL1=0xFD;
	PCON=0x00;
	TR1=1;
	REN=1;
	SM0=0;
	SM1=1;
	EA=1;
	ES=1;
}

void uart_communication() interrupt 4
{
    int buffer;
    ES=0;
    buffer=SBUF;
    RI=0;
    if(status==0){
        command=buffer;
        status=1;
    }else{
        param=buffer;
        status=0;
    };
    if(status==0){
        if(command==0x00){
            switch(param){
                case 0x00:P00=0;break;
                case 0x01:P01=0;break;
                case 0x02:P02=0;break;
                case 0x03:P03=0;break;
                case 0x04:P04=0;break;
                case 0x05:P05=0;break;
                case 0x06:P06=0;break;
                case 0x07:P07=0;break;
                case 0x10:P10=0;break;
                case 0x11:P11=0;break;
                case 0x12:P12=0;break;
                case 0x13:P13=0;break;
                case 0x14:P14=0;break;
                case 0x15:P15=0;break;
                case 0x16:P16=0;break;
                case 0x17:P17=0;break;
                case 0x20:P20=0;break;
                case 0x21:P21=0;break;
                case 0x22:P22=0;break;
                case 0x23:P23=0;break;
                case 0x24:P24=0;break;
                case 0x25:P25=0;break;
                case 0x26:P26=0;break;
                case 0x27:P27=0;break;
                case 0x32:P32=0;break;
                case 0x33:P33=0;break;
                case 0x34:P34=0;break;
                case 0x35:P35=0;break;
                case 0x36:P36=0;break;
                case 0x37:P37=0;break;
            };
        }else if(command==0x01){
            switch(param){
                case 0x00:P00=1;break;
                case 0x01:P01=1;break;
                case 0x02:P02=1;break;
                case 0x03:P03=1;break;
                case 0x04:P04=1;break;
                case 0x05:P05=1;break;
                case 0x06:P06=1;break;
                case 0x07:P07=1;break;
                case 0x10:P10=1;break;
                case 0x11:P11=1;break;
                case 0x12:P12=1;break;
                case 0x13:P13=1;break;
                case 0x14:P14=1;break;
                case 0x15:P15=1;break;
                case 0x16:P16=1;break;
                case 0x17:P17=1;break;
                case 0x20:P20=1;break;
                case 0x21:P21=1;break;
                case 0x22:P22=1;break;
                case 0x23:P23=1;break;
                case 0x24:P24=1;break;
                case 0x25:P25=1;break;
                case 0x26:P26=1;break;
                case 0x27:P27=1;break;
                case 0x32:P32=1;break;
                case 0x33:P33=1;break;
                case 0x34:P34=1;break;
                case 0x35:P35=1;break;
                case 0x36:P36=1;break;
                case 0x37:P37=1;break;
            };
        }else if(command==0x02){
            switch(param){
                case 0x00:SBUF=P00?0x01:0x00;break;
                case 0x01:SBUF=P01?0x01:0x00;break;
                case 0x02:SBUF=P02?0x01:0x00;break;
                case 0x03:SBUF=P03?0x01:0x00;break;
                case 0x04:SBUF=P04?0x01:0x00;break;
                case 0x05:SBUF=P05?0x01:0x00;break;
                case 0x06:SBUF=P06?0x01:0x00;break;
                case 0x07:SBUF=P07?0x01:0x00;break;
                case 0x10:SBUF=P10?0x01:0x00;break;
                case 0x11:SBUF=P11?0x01:0x00;break;
                case 0x12:SBUF=P12?0x01:0x00;break;
                case 0x13:SBUF=P13?0x01:0x00;break;
                case 0x14:SBUF=P14?0x01:0x00;break;
                case 0x15:SBUF=P15?0x01:0x00;break;
                case 0x16:SBUF=P16?0x01:0x00;break;
                case 0x17:SBUF=P17?0x01:0x00;break;
                case 0x20:SBUF=P20?0x01:0x00;break;
                case 0x21:SBUF=P21?0x01:0x00;break;
                case 0x22:SBUF=P22?0x01:0x00;break;
                case 0x23:SBUF=P23?0x01:0x00;break;
                case 0x24:SBUF=P24?0x01:0x00;break;
                case 0x25:SBUF=P25?0x01:0x00;break;
                case 0x26:SBUF=P26?0x01:0x00;break;
                case 0x27:SBUF=P27?0x01:0x00;break;
                case 0x32:SBUF=P32?0x01:0x00;break;
                case 0x33:SBUF=P33?0x01:0x00;break;
                case 0x34:SBUF=P34?0x01:0x00;break;
                case 0x35:SBUF=P35?0x01:0x00;break;
                case 0x36:SBUF=P36?0x01:0x00;break;
                case 0x37:SBUF=P37?0x01:0x00;break;
            };
            while(!TI);
            TI=0;
        };
    };
    ES=1;
}

void main()
{
    uart_init();
    while(1);
}