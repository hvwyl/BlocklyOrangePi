#include<8051.h>

int status=0;

int command=0xFF;
int param=0xFF;

void uart_init()
{
    SCON=0x50;
    TMOD=0x20;
    TH1=0xFD;//Baud 9600
    TL1=0xFD;
    PCON=0x00;
    TR1=1;
    REN=1;
    SM0=0;
    SM1=1;
    EA=1;
    ES=1;
}

void uart_communication() __interrupt 4
{
    int buffer;
    ES=0;
    buffer=SBUF;
    RI=0;
    if(status==0){
        command=buffer;
        status=1;
    }else{
        param=buffer;
        status=0;
    };
    if(status== 0){
        if(command==0x00){
            switch(param){
                case 0x00:P0_0=0;break;
                case 0x01:P0_1=0;break;
                case 0x02:P0_2=0;break;
                case 0x03:P0_3=0;break;
                case 0x04:P0_4=0;break;
                case 0x05:P0_5=0;break;
                case 0x06:P0_6=0;break;
                case 0x07:P0_7=0;break;
                case 0x10:P1_0=0;break;
                case 0x11:P1_1=0;break;
                case 0x12:P1_2=0;break;
                case 0x13:P1_3=0;break;
                case 0x14:P1_4=0;break;
                case 0x15:P1_5=0;break;
                case 0x16:P1_6=0;break;
                case 0x17:P1_7=0;break;
                case 0x20:P2_0=0;break;
                case 0x21:P2_1=0;break;
                case 0x22:P2_2=0;break;
                case 0x23:P2_3=0;break;
                case 0x24:P2_4=0;break;
                case 0x25:P2_5=0;break;
                case 0x26:P2_6=0;break;
                case 0x27:P2_7=0;break;
                case 0x32:P3_2=0;break;
                case 0x33:P3_3=0;break;
                case 0x34:P3_4=0;break;
                case 0x35:P3_5=0;break;
                case 0x36:P3_6=0;break;
                case 0x37:P3_7=0;break;
            };
        }else if(command==0x01){
            switch(param){
                case 0x00:P0_0=1;break;
                case 0x01:P0_1=1;break;
                case 0x02:P0_2=1;break;
                case 0x03:P0_3=1;break;
                case 0x04:P0_4=1;break;
                case 0x05:P0_5=1;break;
                case 0x06:P0_6=1;break;
                case 0x07:P0_7=1;break;
                case 0x10:P1_0=1;break;
                case 0x11:P1_1=1;break;
                case 0x12:P1_2=1;break;
                case 0x13:P1_3=1;break;
                case 0x14:P1_4=1;break;
                case 0x15:P1_5=1;break;
                case 0x16:P1_6=1;break;
                case 0x17:P1_7=1;break;
                case 0x20:P2_0=1;break;
                case 0x21:P2_1=1;break;
                case 0x22:P2_2=1;break;
                case 0x23:P2_3=1;break;
                case 0x24:P2_4=1;break;
                case 0x25:P2_5=1;break;
                case 0x26:P2_6=1;break;
                case 0x27:P2_7=1;break;
                case 0x32:P3_2=1;break;
                case 0x33:P3_3=1;break;
                case 0x34:P3_4=1;break;
                case 0x35:P3_5=1;break;
                case 0x36:P3_6=1;break;
                case 0x37:P3_7=1;break;
            };
        }else if(command==0x02){
            switch(param){
                case 0x00:SBUF=P0_0?0x01:0x00;break;
                case 0x01:SBUF=P0_1?0x01:0x00;break;
                case 0x02:SBUF=P0_2?0x01:0x00;break;
                case 0x03:SBUF=P0_3?0x01:0x00;break;
                case 0x04:SBUF=P0_4?0x01:0x00;break;
                case 0x05:SBUF=P0_5?0x01:0x00;break;
                case 0x06:SBUF=P0_6?0x01:0x00;break;
                case 0x07:SBUF=P0_7?0x01:0x00;break;
                case 0x10:SBUF=P1_0?0x01:0x00;break;
                case 0x11:SBUF=P1_1?0x01:0x00;break;
                case 0x12:SBUF=P1_2?0x01:0x00;break;
                case 0x13:SBUF=P1_3?0x01:0x00;break;
                case 0x14:SBUF=P1_4?0x01:0x00;break;
                case 0x15:SBUF=P1_5?0x01:0x00;break;
                case 0x16:SBUF=P1_6?0x01:0x00;break;
                case 0x17:SBUF=P1_7?0x01:0x00;break;
                case 0x20:SBUF=P2_0?0x01:0x00;break;
                case 0x21:SBUF=P2_1?0x01:0x00;break;
                case 0x22:SBUF=P2_2?0x01:0x00;break;
                case 0x23:SBUF=P2_3?0x01:0x00;break;
                case 0x24:SBUF=P2_4?0x01:0x00;break;
                case 0x25:SBUF=P2_5?0x01:0x00;break;
                case 0x26:SBUF=P2_6?0x01:0x00;break;
                case 0x27:SBUF=P2_7?0x01:0x00;break;
                case 0x32:SBUF=P3_2?0x01:0x00;break;
                case 0x33:SBUF=P3_3?0x01:0x00;break;
                case 0x34:SBUF=P3_4?0x01:0x00;break;
                case 0x35:SBUF=P3_5?0x01:0x00;break;
                case 0x36:SBUF=P3_6?0x01:0x00;break;
                case 0x37:SBUF=P3_7?0x01:0x00;break;
            };
            while(!TI);
            TI=0;
        };
    };
    ES=1;
}

void main()
{
    uart_init();
    while(1);
}
