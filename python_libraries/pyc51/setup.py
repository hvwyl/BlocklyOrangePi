from distutils.core import setup
setup(
    name='pyc51',
    version='1.0',
    description='pyc51 library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    py_modules=['pyc51']
)