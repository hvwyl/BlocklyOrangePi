from distutils.core import setup,Extension
setup(
    name='st7735s',
    version='1.0',
    description='st7735s library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    ext_modules=[
        Extension(
            'st7735s',
            libraries=['wiringPi','crypt','pthread','m','rt'],
            sources=['st7735s.c','tft.c']
        )
    ]
)