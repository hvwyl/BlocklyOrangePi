#ifndef __TFT_H
#define __TFT_H

struct tft{
    char* spidev;
    unsigned int dc;
    unsigned int res;
    unsigned int speed;
    unsigned int width;
    unsigned int height;
    int fd;
};

void TFT_Init(struct tft* config);
void TFT_Reset(struct tft* config);
void TFT_Clear(struct tft* config,unsigned short Color);
void TFT_DrawPoint(struct tft* config,unsigned short x,unsigned short y,unsigned short Color);
void TFT_DrawRect(struct tft* config,unsigned short x1,unsigned short y1,unsigned short x2,unsigned short y2,unsigned short Color);
void TFT_DrawLine(struct tft* config,unsigned short x1,unsigned short y1,unsigned short x2,unsigned short y2,unsigned short Color);
void TFT_DrawChar(struct tft* config,unsigned short x,unsigned short y,unsigned int ascii_code,unsigned short front_color,unsigned short back_color);
void TFT_DrawString(struct tft* config,unsigned short x,unsigned short y,char* string,unsigned short front_color,unsigned short back_color);
void TFT_Close(struct tft* config);

#endif