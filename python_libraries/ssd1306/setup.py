from distutils.core import setup
setup(
    name='ssd1306',
    version='1.0',
    description='ssd1306 library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    py_modules=['ssd1306']
)