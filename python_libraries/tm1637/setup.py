from distutils.core import setup
setup(
    name='tm1637',
    version='1.0',
    description='tm1637 library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    py_modules=['tm1637']
)