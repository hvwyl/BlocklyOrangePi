from distutils.core import setup
setup(
    name='ads1115',
    version='1.0',
    description='ads1115 library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    py_modules=['ads1115']
)