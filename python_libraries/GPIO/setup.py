from distutils.core import setup,Extension
setup(
    name='GPIO',
    version='1.0',
    description='GPIO library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    ext_modules=[
        Extension(
            'GPIO',
            libraries=['wiringPi','crypt','pthread','m','rt'],
            sources=['GPIO.c']
        )
    ]
)