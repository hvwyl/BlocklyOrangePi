from distutils.core import setup,Extension
setup(
    name='ds18b20',
    version='1.0',
    description='ds18b20 library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    ext_modules=[
        Extension(
            'ds18b20',
            libraries=['wiringPi','crypt','pthread','m','rt'],
            sources=['ds18b20.c']
        )
    ]
)