from distutils.core import setup
setup(
    name='lcd1602',
    version='1.0',
    description='lcd1602 library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    py_modules=['lcd1602']
)