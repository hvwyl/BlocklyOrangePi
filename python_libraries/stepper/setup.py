from distutils.core import setup
setup(
    name='stepper',
    version='1.0',
    description='stepper library',
    author='hgc',
    author_email='594352301@qq.com',
    url='https://hgc.com',
    py_modules=['stepper']
)